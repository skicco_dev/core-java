package com.corejava;




import com.corejava.collections.queue.ExampleQueue;
import com.corejava.collections.set.ExampleHashSet;
import com.corejava.collections.set.ExampleSet;
import com.corejava.collections.set.ExampleSortedSet;
import com.corejava.generics.ExampleGenerics;
import com.corejava.generics.ExampleGenericsClass;
import com.corejava.maps.ExampleHashMap;
import com.corejava.maps.ExampleLinkedHashMap;
import com.corejava.maps.ExampleMaps;
import com.corejava.maps.ExampleSortedMapAndTreeMap;
import com.corejava.multithreading.ExampleMultiThreading;

import java.io.FileNotFoundException;

public class Main {

    public static void main(String[] args) throws InterruptedException, FileNotFoundException {

        ExampleMultiThreading T1 = new ExampleMultiThreading("Thread-1 ");
        T1.start();
        ExampleMultiThreading T2 = new ExampleMultiThreading("Thread-2 ");
        T2.start();
    }
}
