package com.corejava.multithreading;

 class PrintDemo {

    public void printCount(){
        try {
            for (int i= 5; i>0; i--){
                System.out.println("Counter #### "+i);
            }
        }
        catch (Exception e){
            System.out.println("The thread is interrupted");
        }
    }

}

class ThreadDemo extends Thread{
    private Thread thread;
    private String threadName;
    PrintDemo PD;

    public ThreadDemo(String threadName, PrintDemo SP) {
        this.threadName = threadName;
        this.PD = PD;
    }

    public void run(){
        PD.printCount();
        System.out.println("Thread "+threadName+" exiting....");

    }

    public void start(){
        System.out.println("Starting "+threadName);
        if(thread ==  null){
            thread = new Thread(this.threadName);
            thread.start();
        }
    }
}

public class SynchronisedProgramming{

     public static void main(String[] args){

         PrintDemo PD = new PrintDemo();
         ThreadDemo T1 = new ThreadDemo("Thread - 1 ", PD);
         ThreadDemo T2 = new ThreadDemo("Thread - 2 ", PD);

         T1.start();
         T2.start();

         try {
             T1.join();
             T2.join();
         } catch (InterruptedException e) {
             e.printStackTrace();
         }
     }
}