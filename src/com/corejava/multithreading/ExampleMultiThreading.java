package com.corejava.multithreading;

public class ExampleMultiThreading implements Runnable{

    private Thread  thread;
    private String threadName;

    public ExampleMultiThreading( String threadName) {
        this.threadName = threadName;
    }


    @Override
    public void run() {

        try {
            for(int i= 4;  i >0; i--){
                System.out.println("Thread "+threadName+" "+i);
                Thread.sleep(50);
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Thread "+threadName+" exiting....");
    }

    public void start(){
        System.out.println("Starting "+threadName);
        if(thread == null){
            thread = new Thread(this,threadName);
            thread.start();
        }
    }
}
