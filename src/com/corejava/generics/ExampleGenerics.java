package com.corejava.generics;

public class ExampleGenerics {

    public static < E > void exampleGenericsMethod(E[] inputArray ){


        for (E e:inputArray
             ) {
            System.out.println(e);
        }
        //return inputArray[0];
    }
}
