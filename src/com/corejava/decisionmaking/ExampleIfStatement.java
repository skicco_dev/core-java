package com.corejava.decisionmaking;

public class ExampleIfStatement {
    String[] fruits = {"Apple", "Banana", "Orange", "Papaya", "Water  melon"};

    public boolean isFruitInFruits(String fruit) {
        for (String fruitFromArray : fruits
        ) {
            if (fruit.equalsIgnoreCase(fruitFromArray)) {
                return true;
            }
        }
        return false;

    }

    public boolean isEven(int number) {

        if (number % 2 == 0) {
            return true;
        } else {
            return false;
        }
    }

    public void testingANumber(int number){

        if(number%3==0){
            if(number % 5==0){
                System.out.println("The number is divided by 3 and 5");
            }else {
                System.out.println("The number is divided by only 3");
            }
        }
        else{
            System.out.println("The number is neither divided by 3 nor by 5");
        }
    }
}
