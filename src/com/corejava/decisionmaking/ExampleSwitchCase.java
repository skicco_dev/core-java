package com.corejava.decisionmaking;

public class ExampleSwitchCase {

    public void printRemarkFromGrade(char grade) {

        String isStudentEligibleForScholarship;

        switch (grade) {
            case 'A':
                System.out.println("excellent");
                break;
            case 'B':
                System.out.println("very good");
                break;
            case 'C':
                System.out.println("Good");
                break;
            case 'D':
                System.out.println("average");
                break;
            case 'E':
                System.out.println("just pass");
                break;
            case 'F':
                System.out.println("fail");
                break;
            default:
                System.out.println("grading is done");
        }

isStudentEligibleForScholarship = (grade=='A')?"yes":"No";
        System.out.println(isStudentEligibleForScholarship);
    }

}
