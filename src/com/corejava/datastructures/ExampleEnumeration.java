package com.corejava.datastructures;

import java.util.*;

public class ExampleEnumeration {

    public void printWeeks(){
        Enumeration daysInAWeek;
        Vector dayNames = new Vector();
        Dictionary dictionary = new Hashtable();
        dayNames.add("Sunday");
        dayNames.add("Monday");
        dayNames.add("Tuesday");
        dayNames.add("Wednesday");
        dayNames.add("Thursday");
        dayNames.add("Friday");
        dayNames.add("Saturday");

        daysInAWeek = dayNames.elements();

        while (daysInAWeek.hasMoreElements()){
            System.out.println(daysInAWeek.nextElement() );
        }

    }
}
