package com.corejava.ExampleStrings;

public class ExampleStrings {

    String name = "This is a string";

    String string = new String();

    String space = " ";

    StringBuilder stringBuilder;
    StringBuffer stringBuffer = new StringBuffer("This is a string buffer");


    String str1 = "abc", str2 = "abcd";

    public void printStringBufferActs() {
        System.out.println(stringBuffer);
        System.out.println(stringBuffer.reverse());
        stringBuffer.delete(5, 10);
        System.out.println(stringBuffer.reverse());
        System.out.println(stringBuffer.replace(5, 12, "replace"));



    }


    public void printStringActs() {

        System.out.println(name.length());
        System.out.println(space.isBlank());
        System.out.println(space.isEmpty());
        System.out.println(name.charAt(5));
        System.out.println(str1.equals(str2));
        System.out.println(str1.concat(str2));
        System.out.println(str1 + str2);
        System.out.println("abcd".concat(str2));
        System.out.println("hello"+" world"+" ..!");
    }

}
