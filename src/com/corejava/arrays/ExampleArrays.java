package com.corejava.arrays;

import java.util.Arrays;

public class ExampleArrays {

    int anInt[] = {1,2,3,4,5,6,7,8,9};
    int[] ints = {1,2,3,4,5,6,7,8,9}; //preferable
    String[] names = new String[10];
    String[] cities ={"Hyderabad", "Delhi","Kolkata","Mumbai","Newyork","London"};


    public void exampleArrayMethods(){

        for (int i =0;i<cities.length; i ++){
            System.out.println(cities[i]);
        }

        for (String city: cities
             ) {
            System.out.println(city);
        }
    }

}
