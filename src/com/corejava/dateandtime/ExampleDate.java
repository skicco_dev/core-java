package com.corejava.dateandtime;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class ExampleDate {

    public void printCurrentDateAndTime() throws InterruptedException {

        Date now = new Date();
        System.out.println(now);

        Date later = new Date();
        System.out.println(later);
        System.out.println(later.after(now));
        System.out.println(now.before(later));
        System.out.println(now.equals(later));
        System.out.println(now.getTime());
        System.out.println(now.toString());
    }


    public void printFormattedDate(){

        Date date = new Date();
        SimpleDateFormat format= new SimpleDateFormat(" E hh:mm:ss dd/MM/yyyy a zzz");
        System.out.println(format.format(date));
        GregorianCalendar gregorianCalendar = new
                GregorianCalendar();
        System.out.println(gregorianCalendar);

    }
}
