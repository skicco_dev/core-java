package com.corejava.loops;

public class ExampleDoWhileLoop {

    int x =10;

    public void ExampleDoWhileLoop(){

        do {
            System.out.println(x);
            x++;
        }
        while (x<=20);

    }
}
