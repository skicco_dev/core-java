package com.corejava.loops;

public class LoopControl {

    public void continueExample(){
        for (int i = 0; i < 35; i++) {
            if(i == 10){
                continue;
            }

            System.out.println(i);
        }
    }

    public void breakExample(){
        for (int i = 0; i < 35; i++) {
            if(i == 10){
                break;
            }

            System.out.println(i);
        }

    }
}
