package com.corejava.loops;

public class ExampleForloops {

    String[] cities = {"Mumbai","Delhi","Hyderabad","Chennai","Culcatta","Ahmedabad"};

    public void printCities(){

        for (int i = 0;i <cities.length; i++){
            System.out.println(cities[i]);
        }
    }

    public void printCitiesByforEach(){

        for (String city: cities){
            System.out.println("This is from for each loop");
            System.out.println(city);
        }
    }
}
