package com.corejava.loops;

public class ExampleWhileLoop {

    int x = 10;

    public void exampleWhileLoop() {

        while (x < 30) {
            System.out.println(x);
            x++;
        }
    }
}

