package com.corejava.wrapperclasses;

public class ExampleWrapperClasses {

   Number number = new Integer(10);
   public void exampleNumbers(){
       System.out.println(number.floatValue());
       System.out.println(number.byteValue());
       System.out.println(number.doubleValue());
       System.out.println(number.longValue());

   }
}
