package com.corejava.operators;

public class ExampleRelationalOperators {

    public boolean equals(int a, int b) {
        return a == b;
    }

    public boolean notEquals(int a, int b) {
        return a != b;
    }

    public boolean greater(int a, int b) {
        return a > b;
    }

    public boolean lessThan(int a, int b) {
        return a < b;
    }

    public boolean greaterThanEquals(int a, int b) {
        return a >= b;
    }

    public boolean lessThanEquals(int a, int b) {
        return a <= b;
    }
}
