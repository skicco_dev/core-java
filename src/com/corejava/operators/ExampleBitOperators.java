package com.corejava.operators;

public class ExampleBitOperators {

    public int bitwiseAnd(int a, int b){
        return a&b;
    }

    public int bitwiseOr(int a, int b){
        return a|b;
    }

    public int bitwiseXor(int a, int b){
        return a^b;
    }

    public int bitwiseCompliment(int a){
        return ~a;
    }

    public int bitwiseleftShift(int a){
        return a<<2;
    }

    public int bitwiseRightShift(int a){
        return a>>2;
    }

    public int bitwiseZeroFill(int a){
        return a>>>2;
    }
}
