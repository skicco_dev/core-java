package com.corejava.operators;

public class ExampleAssignmentOperators {

    int x = 10;
    int y = 20;
    Integer z = 40;

    public void ExampleAssignmentOperators() {

        System.out.println(x += y); // x = x+y
        System.out.println(x -= y); // x = x-y
        System.out.println(x *= y); // x = x*y
        System.out.println(x /= y); // x = x/y
        System.out.println(x %= y); // x = x%y
        System.out.println(x <<= y); // x = x<<y
        System.out.println(x >>= y); // x = x>>y
        System.out.println(x &= y); // x = x&y
        System.out.println(x ^= y); // x = x<^y
        System.out.println(x |= y); // x = x|y


    }

    public void exampleMiscellaneosOperator() {

        double temperature = 35.50;

        String season = (temperature >= 35) ? "summer" : "Non summer";
        System.out.println(season);

    }

    public boolean checkInstance(){

      return z instanceof Integer;
    }
}
