package com.corejava.operators;

public class ExampleLogicalOperators {


    public boolean logicalAnd(boolean a, boolean b){
        return a && b;
    }

    public boolean logicalOr(boolean a, boolean b){
        return a || b;
    }

    public boolean logicalNot(boolean a, boolean b){
        return !(a && b);
    }
}
