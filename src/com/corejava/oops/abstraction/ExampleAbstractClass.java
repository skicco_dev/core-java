package com.corejava.oops.abstraction;

public abstract class ExampleAbstractClass {

    public abstract String exampleAbstractMethod();

}
