package com.corejava.oops;

public class ExampleMethodOverloading {

    public double sum(int a, double b){
        return a+b;
    }

    public double sum(double a, int b){
        return a+b;
    }
    public double sum(int a, int b,double c){
        return a+b+c;
    }
}
