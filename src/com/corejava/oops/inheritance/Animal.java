package com.corejava.oops.inheritance;

public class Animal {

    private String name;

    public Animal() {
        System.out.println("calling animal class");
    }

    public void exampleMethod() {
        System.out.println("This is animal class");
    }

    public void makeSound(){
        System.out.println("Animal will make sound");
    }
}
