package com.corejava.oops.inheritance;

public class Deer extends Mammal {

    public Deer() {
        System.out.println("This is Deer class");
    }
}
