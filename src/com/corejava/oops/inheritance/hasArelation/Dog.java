package com.corejava.oops.inheritance.hasArelation;

import com.corejava.oops.inheritance.Animal;

public class Dog extends Animal {

    public void makeSound(){
        System.out.println("dog will bark");
    }
}
