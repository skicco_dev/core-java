package com.corejava.oops.inheritance;

public class ExampleParent {

    private int x = 10;
    public static final String APP_NAME = "Java";

    public ExampleParent(int x) {
        this.x = x;
    }

    public void printAppName() {
        System.out.println(APP_NAME);
    }
}
