package com.corejava.oops.inheritance;

public class ExampleChild extends ExampleParent {

    public ExampleChild(int x) {
        super(x);
    }

    public void printHelloWorld() {
        System.out.println("Hello world");
    }

    public void printAppName() {
        System.out.println(APP_NAME);
    }

    public String getAppName() {
        this.printAppName();
        super.printAppName();
        return super.APP_NAME;

    }
}
