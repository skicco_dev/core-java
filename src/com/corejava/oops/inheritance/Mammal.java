package com.corejava.oops.inheritance;

public class Mammal extends Animal {

    public Mammal() {
        System.out.println("This is mammal class");
    }

    public void exampleMethod() {
        System.out.println("This is mammal class");
    }
}
