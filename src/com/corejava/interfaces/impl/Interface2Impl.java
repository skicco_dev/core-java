package com.corejava.interfaces.impl;

import com.corejava.interfaces.ExampleInterface;
import com.corejava.interfaces.Interface1;
import com.corejava.interfaces.Interface2;

public class Interface2Impl implements Interface2, Interface1, ExampleInterface {
    @Override
    public void printX() {

    }

    @Override
    public String getX() {
        return null;
    }
}
