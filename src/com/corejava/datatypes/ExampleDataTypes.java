package com.corejava.datatypes;

public class ExampleDataTypes {

    private byte aByte = 127;
    private short aShort = 1236;
    private int anInt = 123365478;
    private long aLong = 1236985522214144l;
    private float aFloat = 100F;
    private double aDouble = 111.223;
    private char aChar = 'H';
    private boolean aBoolean = true;

    //Default constructor
    public ExampleDataTypes() {

        System.out.println("This is a default constructor for ExampleDataTypes");
    }

    //Parameterized constructor
    public ExampleDataTypes(byte aByte, short aShort, int anInt, long aLong, float aFloat, double aDouble, char aChar, boolean aBoolean) {
        System.out.println("This is a parameterized constructor for ExampleDataTypes");
        this.aByte = aByte;
        this.aShort = aShort;
        this.anInt = anInt;
        this.aLong = aLong;
        this.aFloat = aFloat;
        this.aDouble = aDouble;
        this.aChar = aChar;
        this.aBoolean = aBoolean;
    }

    public void printData() {
        System.out.println(aByte);
        System.out.println(aShort);
        System.out.println(anInt);
        System.out.println(aLong);
        System.out.println(aFloat);
        System.out.println(aDouble);
        System.out.println(aChar);
        System.out.println(aBoolean);
    }
}
