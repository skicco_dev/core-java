package com.corejava.datatypes;

public class ExampleVariables {
    private int num;

    //constant
    public static final int MAX_VALUE= 100;

    public void printLocalVariables() {

        //local variables
        int i = 20;
        String name = "Bob";
        double length = 20.23D;
        num = 10;
        System.out.println(i);
        System.out.println(name);
        System.out.println(length);
        System.out.println(age);
        System.out.println(num);


    }

    //instance variable
    public  int age = 4;

    public void printInstanceVariables() {
        num = 7;
        System.out.println(age);
        System.out.println(num);

    }

}
