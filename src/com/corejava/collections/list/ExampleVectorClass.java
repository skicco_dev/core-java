package com.corejava.collections.list;

import java.util.Vector;

public class ExampleVectorClass {

    // similar to array list but vector is for the thread safe and synchronised
    Vector<String> strings = new Vector<>();

}
