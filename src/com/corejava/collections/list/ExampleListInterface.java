package com.corejava.collections.list;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ExampleListInterface {
    List<Integer> integers = new ArrayList<>();

    public void exampleList() {
        System.out.println(integers.size());
        System.out.println(integers.isEmpty());
        Collection collection = new ArrayList();
        collection.add(544);
        collection.add(4);
        collection.add(5);
        integers.add(2);
        System.out.println(integers.addAll(1, collection));
        System.out.println(integers);
        System.out.println(integers.containsAll(collection));
        integers.retainAll(collection);
        System.out.println(integers);
        System.out.println(integers.size());
        System.out.println(integers.isEmpty());
        System.out.println("This list contains 4 : " + integers.contains(4));


    }
}
