package com.corejava.collections.list;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;

public class ExampleLinkedList {

    LinkedList<Integer> integers = new LinkedList<>();
    Collection<Integer> collection = new ArrayList<>();

    public void printLinkedList() {

        collection.add(1001);
        collection.add(1002);
        collection.add(1003);
        collection.add(1004);
        collection.add(1005);

        integers.add(1);
        integers.add(4);
        integers.add(7);
        integers.add(8);
        integers.add(2);
        integers.add(6);
        System.out.println(integers);
        integers.add(4,9);

        System.out.println(integers);
        integers.addAll(2,collection);
        System.out.println(integers);
        System.out.println(integers.getFirst());
        System.out.println(integers.getLast());

    }

}
