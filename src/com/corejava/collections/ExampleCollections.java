package com.corejava.collections;

import com.corejava.model.User;

import java.util.ArrayList;
import java.util.Collection;

public class ExampleCollections {

    Collection<Integer> stringCollection = new ArrayList<>();
    Collection<Integer> passonCollection = new ArrayList<>();
    Collection<User> users = new ArrayList<>();

    public void collectionExample() {
        stringCollection.add(15);
        stringCollection.add(4);
        stringCollection.add(14545);
        stringCollection.add(45);
        stringCollection.add(456);
        stringCollection.add(87);

        passonCollection.add(45);
        passonCollection.add(456);
        passonCollection.add(87);
        System.out.println(stringCollection);
        stringCollection.addAll(passonCollection);

        System.out.println(stringCollection);
        stringCollection.clear();
        System.out.println(stringCollection);


    }
}
