package com.corejava.collections.set;

import java.util.AbstractSet;
import java.util.HashSet;
import java.util.Set;

public class ExampleSet {

    Set<String> cities = new HashSet<>();

    public void printCities(){

        AbstractSet<String> states = new HashSet<>();

        cities.add("Mumbai");
        cities.add("Hyderabad");
        cities.add("Delhi");
        cities.add("Kolkata");
        cities.add("Hyderabad");
        cities.add("Mumbai");

        System.out.println(cities);
        System.out.println(cities.hashCode());
    }
}
