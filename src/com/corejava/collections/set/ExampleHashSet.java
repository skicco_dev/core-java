package com.corejava.collections.set;

import java.util.*;

public class ExampleHashSet {

    Collection<String> randomCountries = new ArrayList<>();

    public void printHashSet() {

        randomCountries.add("India");
        randomCountries.add("USA");
        randomCountries.add("Sweden");
        randomCountries.add("Russia");
        randomCountries.add("France");
        randomCountries.add("Germany");
        randomCountries.add("Sweden");
        randomCountries.add("Germany");
        System.out.println(randomCountries);
        Set<String> countries = new HashSet<>(randomCountries);
        Set<String> orderedCountries = new LinkedHashSet<>(randomCountries);
        System.out.println(countries);
        System.out.println(orderedCountries);
        countries.add("Finland");
        System.out.println(countries);
        System.out.println(countries.hashCode());
        System.out.println(countries.iterator().next().hashCode());
        System.out.println(countries.contains("India"));
    }
}
