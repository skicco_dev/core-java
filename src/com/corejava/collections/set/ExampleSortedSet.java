package com.corejava.collections.set;

import java.util.SortedSet;
import java.util.TreeSet;

public class ExampleSortedSet {
    SortedSet<Integer> integers = new TreeSet<>();
    public void printSortedElements(){

        integers.add(5);
        integers.add(245);
        integers.add(587);
        integers.add(587675);
        integers.add(1);
        integers.add(4);
        integers.add(63834);
        integers.add(5);
        integers.add(245);
        integers.add(587);
        integers.add(587675);
        integers.add(1);
        System.out.println(integers);
        System.out.println(integers.first());
        System.out.println(integers.headSet(12));
        System.out.println(integers.tailSet(12));
        System.out.println(integers.last());
    }
}
