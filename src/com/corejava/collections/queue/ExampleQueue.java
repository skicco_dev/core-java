package com.corejava.collections.queue;

import java.util.PriorityQueue;
import java.util.Queue;

public class ExampleQueue {


    Queue<String> queue = new PriorityQueue<>();

    public void queueOPerations(){
        queue.add("Mango");
        queue.add("Apple");
        queue.add("Banana");
        queue.add("Water melon");
        queue.add("Orange");
        queue.add("Papaya");
        System.out.println(queue);
        System.out.println(queue.peek());
        System.out.println(queue);
        System.out.println(queue.poll());
        System.out.println(queue);

    }
}
