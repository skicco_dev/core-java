package com.corejava.exceptionsexample;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class ExampleExceptions {

    public void checkedException() throws FileNotFoundException {

        File file = new File("D://file.txt");
        FileReader reader = new FileReader(file);
    }

    public void unCheckedException() {
        try {
            int[] numbers = {1, 4, 23, 5, 5, 8, 8, 66, 22};
            System.out.println(numbers[15]);
        } catch (ArrayIndexOutOfBoundsException arrayIndexOutOfBoundsException) {
        } catch (Exception e) {
        }
    }
}
