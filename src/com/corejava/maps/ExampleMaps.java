package com.corejava.maps;

import java.util.HashMap;
import java.util.Map;

public class ExampleMaps {

    Map<Integer, String> integerStringMap = new HashMap<>();
    public void OperationsOnMap(){

        integerStringMap.put(1,"Zara");
        integerStringMap.put(2,"Kyle");
        integerStringMap.put(3,"Mitchell");
        integerStringMap.put(4,"Eva");
        integerStringMap.put(5,"Dennis");
        System.out.println(integerStringMap);
        System.out.println(integerStringMap.size());
        System.out.println(integerStringMap.isEmpty());
        /* integerStringMap.clear();
        System.out.println(integerStringMap.isEmpty());*/
        System.out.println(integerStringMap.containsKey(4));
        System.out.println(integerStringMap.containsKey(41));
        System.out.println(integerStringMap.get(4));

    }
}
