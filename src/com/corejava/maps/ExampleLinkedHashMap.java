package com.corejava.maps;

import java.util.LinkedHashMap;
import java.util.Map;

public class ExampleLinkedHashMap {


    Map<String, String> citiesAndState = new LinkedHashMap(20,0.7F,true);
    public void printCitiesAndState(){

        citiesAndState.put("Delhi","Delhi");
        citiesAndState.put("Mumbai","Maharashtra");
        citiesAndState.put("Kolkata","West bengal");
        citiesAndState.put("Hyderabad","Telangana");
        citiesAndState.put("Pune","Maharastra");
        citiesAndState.put("Chennai","Tamilnadu");

        System.out.println(citiesAndState);
        System.out.println(citiesAndState.get("Mumbai"));
        System.out.println(citiesAndState.containsKey("Mumbai"));
        System.out.println(citiesAndState.containsValue("Maharastra"));
        System.out.println(citiesAndState);
    }
}
