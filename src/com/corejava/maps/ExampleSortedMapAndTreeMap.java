package com.corejava.maps;

import java.util.SortedMap;
import java.util.TreeMap;

public class ExampleSortedMapAndTreeMap {

    SortedMap<Integer, String> cities = new TreeMap<>();

    public void printAllCities(){
        cities.put(500001,"Hyderabad");
        cities.put(200001,"Mumbai");
        cities.put(600001,"Chennai");
        cities.put(300001,"Kolkata");

        for(int i = 100001; i <=999999; i++){
            cities.put(i,"xxxxxx");
            cities.put(i-1,"yyyyyyy");

        }

        System.out.println(cities);
        System.out.println(cities.firstKey());
        System.out.println(cities.lastKey());
        System.out.println(cities.size());
        System.out.println(cities.tailMap(888888));
    }


}
