package com.corejava.maps;

import java.util.HashMap;

public class ExampleHashMap {


    HashMap<Integer, String>  students = new HashMap<>();

    public void printAllStudents(){

        students.put(1001, "Rahul");
        students.put(1002, "Priya");
        students.put(1003, "Ravi");
        students.put(1004, "Harshad");
        students.put(1005, "Joseph");
        students.put(1004, "Siraj");


        System.out.println(students);
        System.out.println(students.keySet());
        System.out.println(students.values());
    }
}
